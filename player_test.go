package main

import (
	"testing"
)

var PList = []Player{
	{Name: "Sam", ID: 1},
	{Name: "Bob", ID: 2},
	{Name: "Max", ID: 3},
	{Name: "Green Shadow", ID: 25},
}

func TestFindPlayer(t *testing.T) {

	p, err := findbyID(PList, 4)
	if err != ErrNoPlayerID {
		t.Errorf("We found a player %+v when we should not, with error message %s", p, err)
	}

	p, err = findbyID(PList, 25)
	if p.Name != "Green Shadow" {
		t.Errorf("Instead of player Green Shadow, ID 25, was found %+v, error is %s", p, err)
	}
}

func TestDeclareWarAndPeace(t *testing.T) {
	initPlayers(PList)
	PList[1].DeclareWar(2)
	PList[2].DeclareWar(2)
	for k, v := range PList[1].Relations {
		if !v.atWar {
			p, err := findbyID(PList, k)
			if err != nil {
				t.Fatal("This is madness with searching for players")
			}
			t.Errorf("Bob is at peace with %+s, when he should be at war", p.Name)
		}
	}
	for k, v := range PList[2].Relations {
		if k == 2 && !v.atWar {
			t.Errorf("Max is at peace with Bob, but should be at war")
		}
		if k != 2 && v.atWar {
			p, err := findbyID(PList, k)
			if err != nil {
				t.Fatal("This is madness with searching for players")
			}
			t.Errorf("Max is at war with %s, should be at peace", p.Name)
		}
	}

	PList[1].DeclarePeace(3)
	PList[2].DeclarePeace(3)
	for k, v := range PList[1].Relations {
		if k == 3 && v.atWar {
			t.Errorf("Bob is at war with Max, but should be at peace")
		}
		if k != 3 && !v.atWar {
			p, err := findbyID(PList, k)
			if err != nil {
				t.Fatal("This is madness with searching for players")
			}
			t.Errorf("Bob is at peace with %s, should be at war", p.Name)
		}
	}
	for k, v := range PList[2].Relations {
		if v.atWar {
			p, err := findbyID(PList, k)
			if err != nil {
				t.Fatal("This is madness with searching for players")
			}
			t.Errorf("Max is at war with %+s, when he should be at peace", p.Name)
		}
	}
}
