package main

import "testing"

func TestDefense(t *testing.T) {
	a := Ship{
		TL:       tech,
		Drive:    8,
		Armament: 1,
		Weapons:  8,
		Shield:   8}
	b := Ship{
		TL:       tech,
		Drive:    1,
		Armament: 1,
		Weapons:  1,
		Shield:   1}
	if a.defense() != 4*b.defense() {
		t.Errorf("Bigger ship %+v should be four times as defended as smaller %+v, but actually defense is %f and %f", a, b, a.defense(), b.defense())
	}
}
func TestMass(t *testing.T) {
	var cases = []struct {
		s    Ship
		want Mass
	}{
		{Ship{TL: TL{Cargo: 1}, Cargo: 1}, 1},
		{Ship{}, 0},
		{Ship{Drive: 1}, 1},
		{Ship{TL: TL{Cargo: 1}, Cargo: 1, hold: 1}, 1},
		{Ship{TL: TL{Cargo: 2}, Cargo: 1, hold: 1}, 1},
		{Ship{TL: TL{Cargo: 2}, Cargo: 1, hold: 2}, 1},
		{Ship{Armament: 1, Weapons: 1}, 1},
		{Ship{Armament: 3, Weapons: 1}, 2},
		{Ship{Armament: 3, Weapons: 1.2}, 2.4},
	}

	for _, shp := range cases {
		got := shp.s.Mass()
		if got != shp.want {
			t.Errorf("Empty mass of the ship was miscalculated, was %f, expected %f from %+v", got, shp.want, shp.s)
		}
	}
}

func TestTotalMass(t *testing.T) {
	var cases = []struct {
		s    Ship
		want Mass
	}{
		{Ship{TL: TL{Cargo: 1}, Cargo: 1}, 1},
		{Ship{}, 0},
		{Ship{Drive: 1}, 1},
		{Ship{TL: TL{Cargo: 1}, Cargo: 1, hold: 1}, 2},
		{Ship{TL: TL{Cargo: 2}, Cargo: 1, hold: 1}, 1.5},
		{Ship{TL: TL{Cargo: 2}, Cargo: 1, hold: 2}, 2},
		{Ship{Armament: 1, Weapons: 1}, 1},
		{Ship{Armament: 3, Weapons: 1}, 2},
		{Ship{Armament: 3, Weapons: 1.2}, 2.4},
	}

	for _, shp := range cases {
		got := shp.s.Total()
		if got != shp.want {
			t.Errorf("Total mass of the ship was miscalculated, was %f, expected %f from %+v", got, shp.want, shp.s)
		}
	}
}

func TestProbability(t *testing.T) {
	s := Ship{
		TL:       tech,
		Drive:    10,
		Armament: 1,
		Weapons:  10,
		Shield:   10}
	p := probability(&s, &s)

	if p != 0.5 {
		t.Errorf("Probability of destroying equal for ship with %+v should be 0.5, but it's %f", s, p)
	}
	h := s
	h.TL.Weapons = 4
	hp := probability(&h, &s)
	if hp != 1 {
		t.Logf("%f, %f", h.power(), s.defense())
		t.Errorf("Probability of destroying ship with %+v for ship with %+v should be 1, but it's %f", h, s, hp)
	}

	l := s
	l.TL.Shields = 4
	lp := probability(&s, &l)
	if lp != 0 {
		t.Logf("%f, %f", s.power(), l.defense())
		t.Errorf("Probability of destroying ship with %+v for ship with %+v should be 0, but it's %f", l, s, lp)
	}

	d := s
	d.Armament = 0
	dp := probability(&d, &s)
	if dp != 0 {
		t.Logf("%f, %f", h.power(), s.defense())
		t.Errorf("Probability of destroying ship with %+v for ship with %+v should be 0, but it's %f", s, s, dp)
	}
}

func TestEqualShips(t *testing.T) {
	cases := []struct {
		a      Ship
		b      Ship
		result bool
	}{
		{
			Ship{
				TL:       tech,
				Drive:    8,
				Armament: 1,
				Weapons:  8,
				Shield:   8},
			Ship{
				TL:       tech,
				Drive:    8,
				Armament: 1,
				Weapons:  8,
				Shield:   8},
			true,
		},
		{
			Ship{
				TL:       tech,
				Drive:    8,
				Armament: 1,
				Weapons:  8,
				Shield:   8},
			Ship{
				TL:       tech2,
				Drive:    8,
				Armament: 1,
				Weapons:  8,
				Shield:   8},
			false,
		},
		{
			cover,
			cover2,
			false,
		},
		{
			cover,
			cover,
			true,
		},
		{
			cover,
			heavycover,
			false,
		},
		{
			Ship{},
			Ship{},
			true,
		},
	}
	for _, c := range cases {
		if c.a.Equals(c.b) != c.result {
			if c.result {
				t.Errorf("Ships %+v and %+v are not equal when they should be", c.a, c.b)
			} else {
				t.Errorf("Ships %+v and %+v are equal when they not should be", c.a, c.b)
			}
		}
	}
}

func TestLessTL(t *testing.T) {
	cases := []struct {
		a TL
		b TL
		r bool
	}{
		{TL{}, TL{Weapons: 1}, true},
		{TL{Weapons: 1}, TL{}, false},
		{TL{}, TL{Cargo: 1}, true},
		{TL{}, TL{Drive: 1}, true},
		{TL{Shields: 1}, TL{}, false},
		{TL{Shields: 1}, TL{Weapons: 1}, true},
		{TL{Weapons: 1}, TL{Drive: 1}, false},
	}
	for _, c := range cases {
		if c.a.less(c.b) != c.r {
			t.Errorf("It is not %t that %+v is less that %+v", c.r, c.a, c.b)
		}
	}
}

func TestShipSpeed(t *testing.T) {
	var cases = []struct {
		s    Ship
		want Speed
	}{
		{Ship{TL: TL{Cargo: 1, Drive: 1}, Cargo: 1, Drive: 1}, 10},
		{Ship{TL: TL{Drive: 1}, Drive: 1}, 20},
		{Ship{TL: TL{Drive: 5}, Drive: 1}, 100},
		{Ship{TL: TL{Cargo: 1, Drive: 1}, Cargo: 1, Drive: 1, hold: 1}, 20. / 3},
		{Ship{TL: TL{Cargo: 1, Drive: 3}, Cargo: 1, Drive: 1, hold: 1}, 20},
		{Ship{TL: TL{Cargo: 1, Drive: 1}, Cargo: 1, hold: 1}, 0},
		{Ship{TL: TL{Cargo: 1, Drive: 3}, Cargo: 1, hold: 1}, 0},
		{Ship{TL: TL{Cargo: 2, Drive: 1}, Cargo: 1, Drive: 1, hold: 1}, 8},
		{Ship{TL: TL{Cargo: 2, Drive: 1}, Cargo: 1, Drive: 1, hold: 2}, 20. / 3},
		{Ship{TL: TL{Drive: 1}, Armament: 1, Weapons: 1, Drive: 1}, 10},
		{Ship{TL: TL{Drive: 1}, Armament: 1, Weapons: 1}, 0},
		{Ship{TL: TL{Drive: 1}, Armament: 3, Weapons: 1, Drive: 1}, 20. / 3},
		{Ship{TL: TL{Drive: 1}, Armament: 3, Weapons: 1.2, Drive: 1}, 20. / 3.4},
	}
	for _, shp := range cases {
		got := shp.s.Speed()
		if got != shp.want {
			t.Errorf("Speed of the ship was miscalculated, was %f, expected %f from %+v with mass %f", got, shp.want, shp.s, shp.s.Mass())
		}
	}
}
