package main

//"os"

// TurnNumber is a number of turns that passed since the start of the game
type TurnNumber int

// Galaxy is the state of the game before orders are executed
// Should be immutable, with new Galaxy being produced every turn
type Galaxy struct {
	Seed         int64
	Turn         TurnNumber
	Name         string
	Size         Dims
	Players      map[PlayerID]Player
	Planets      map[PlanetID]Planet
	Fleets       []Fleet
	FutureOrders []OrderList
}

// Game loop TODO
func Game() {}

// ComputeTurn is one turn of the game. It takes players` orders and
// returns the galaxy at the start of next turn. Phases of the turn
// are as follow:
//  1. Ships are merged into ship groups
//  2. Fleet combat
//  3. Trade routes are processes: goods are loaded into ships at the
//     beginning of trade route and ships depart to hyperspace
//  4. Ships fly through hyperspace
//  5. Ships are merged into ship groups
//  6. Fleet combat after ships exit hyperspace
//  7. Planetary bombardment
//  8. Planets produce new ships
//  9. Ships are merged into ship groups
//  10. Planets produce material, industry or technology
//  11. Cargo is unloaded
//  12. Production level recalculation
//  13. Routes that are outside ship range are culled
//  14. Vote count
//
// Each turn has it's own pseuro-random seed and should run deterministically.
func (state Galaxy) ComputeTurn() (next Galaxy) {

	var newGalaxy = state

	var newFleet = make([]Fleet, len(state.Fleets))
	for _, fleet := range state.Fleets {
		newFleet = append(newFleet, fleet.Compact())
	}

	newGalaxy.Fleets = newFleet

	newGalaxy = newGalaxy.RunCombat()

	//TODO: Clone galaxy, mutate then
	temp := state.applyOrders()

	temp.Seed = state.Seed
	temp.Turn = state.Turn + 1

	return temp
}

func (g Galaxy) applyOrders() Galaxy {
	tg := g
	for i, ol := range g.FutureOrders {
		if ol.Turn != g.Turn {
			continue
		}
		for _, o := range ol.Orders {
			o.Apply(tg)
		}
		tg.FutureOrders = make([]OrderList, len(g.FutureOrders))
		tg.FutureOrders = append(tg.FutureOrders, g.FutureOrders[:i]...)
		tg.FutureOrders = append(tg.FutureOrders, g.FutureOrders[i+1:]...)
	}
	return tg
}

func (state Galaxy) RunCombat() (next Galaxy) {
	/*
	   Корабли, находящиеся  в гиперпространстве, не могут быть атакованы.
	   Если на  планете встречаются  вооруженные корабли  враждующих рас    -
	   происходит сражение.  Уместно заметить,  что к моменту начала сражений
	   корабли, отосланные  с планеты  вручную или  по маршруту,  уже вошли в
	   гиперпространство  и   участия  в  сражениях  не  принимают.  Выглядит
	   сражение так:

	      Случайным  образом  из  всех  участвующих  в  сражении  вооруженных
	   кораблей  выбирается  один.  Он  случайным  образом  выбирает  себе  в
	   качестве мишени вражеский корабль и стреляет по нему. Цель может быть,
	   а может  и не  быть уничтожена,  что зависит  от вооружения,  защиты и
	   Фортуны. Если атакующий корабль еще имеет незадействованное вооружение
	   (имеет несколько  пушек, а  не одну),  он снова  стреляет по  случайно
	   выбранной цели,  и так  повторяется до тех пор, пока они (либо пушки -
	   либо цели) еще есть. Затем опять случайным образом выбирается корабль,
	   которому пришла  пора стрелять.  Так продолжается  до тех пор, пока не
	   отстреляются все  корабли. Если  после этого  еще остались корабли и с
	   той и  с другой  стороны, все  повторяется с самого начала (происходит
	   еще один цикл сражения). Сражение заканчивается только тогда, когда на
	   поле  битвы  не  остается  враждующих  кораблей  способных  уничтожить
	   корабль противника.*/
	return
}
