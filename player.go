package main

import (
	"errors"
)

//PlayerID is unique player id for a session
type PlayerID int

//Player contains everything there is important about a player in session
type Player struct {
	Name      string
	ID        PlayerID
	Relations map[PlayerID]Relation
}

//Relation between this player and some other
type Relation struct {
	atWar bool
}

func initPlayers(pl []Player) {
	for i := range pl {
		pl[i].Relations = make(map[PlayerID]Relation)
		for j := range pl {
			if i == j {
				continue
			}
			pl[i].Relations[pl[j].ID] = Relation{atWar: false}
		}
	}
}

//ErrNoPlayerID appears when no player with such id was found in player list
var ErrNoPlayerID = errors.New("No such player")

func findbyID(plist []Player, id PlayerID) (Player, error) {
	for _, p := range plist {
		if p.ID == id {
			return p, nil
		}
	}
	return Player{}, ErrNoPlayerID
}

//DeclareWar against Player with specified ID or all players if ID is of current player
func (p *Player) DeclareWar(ID PlayerID) {
	if ID == p.ID {
		for k := range p.Relations {
			t := p.Relations[k]
			t.atWar = true
			p.Relations[k] = t
		}
		return
	}
	t := p.Relations[ID]
	t.atWar = true
	p.Relations[ID] = t
}

//DeclarePeace against Player with specified ID or all players if ID is of current player
func (p *Player) DeclarePeace(ID PlayerID) {
	if ID == p.ID {
		for k := range p.Relations {
			t := p.Relations[k]
			t.atWar = false
			p.Relations[k] = t
		}
		return
	}
	t := p.Relations[ID]
	t.atWar = false
	p.Relations[ID] = t
}
