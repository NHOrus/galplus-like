package main

import (
	"testing"
)

func TestPlanetView(t *testing.T) {
	cases := []struct {
		p  Planet
		pv PlanetView
	}{
		{
			Planet{size: 20000, pop: 20000, col: 150},
			PlanetView{size: 200, pop: 200, col: 1},
		},
	}
	for _, c := range cases {
		v := c.p.View()
		if v != c.pv {
			t.Errorf("Planet %+v does not get planet view %+v", c.p, c.pv)
		}
	}
}
