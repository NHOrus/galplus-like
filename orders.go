package main

type OrderList struct {
	Turn   TurnNumber
	Orders []Order
}

type Order interface {
	Apply(Galaxy)
	Player() PlayerID
}

type OrderRename struct {
	Issuer PlayerID
	Target PlanetID
	Name   string
}

func (o OrderRename) Player() PlayerID {
	return o.Issuer
}

func (o OrderRename) Apply(g Galaxy) {
	if g.Planets[o.Target].Owner != o.Issuer {
		return
	}
	tmp := g.Planets[o.Target]
	tmp.Name = o.Name
	g.Planets[o.Target] = tmp
}
