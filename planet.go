package main

//PlanetID is session-wide unique planetary ID, for purposes of being map key
type PlanetID int

//Planet in the galaxy, center of infrastructure and places where all the action happens
//Everything here is calculated to two decimal places
type Planet struct {
	size   int
	wealth int
	ID     PlanetID
	Owner  PlayerID
	Name   string
	mat    int
	ind    int
	pop    int
	col    int
}

//Productivity of a planet, calculated as industry*0.75 + population * 0.25
type Productivity int

//CargoKind is a kind of a cargo being moved in transports to and from planets
type CargoKind int

//Possible values are NONE, empty holds, MATERIALS, resources to be used in building
//of ships, INDUSTRY to procude things with and COLONISTS who are doing producing.
const (
	NONE CargoKind = iota
	MATERIAL
	INDUSTRY
	COLONISTS
)

func (p Planet) prod() Productivity {
	ind := p.ind
	if ind > p.pop {
		ind = p.pop
	}
	return Productivity(3*ind+p.pop) / 4
}

//IndProducion returns amount of industrial production that could be used
//for different purposes after calculating effort spent on material production
//and how much material was used.
func (p Planet) IndProducion() (newInd, usedMat int) {
	maxInd := int(p.prod()) / 5
	if p.mat >= maxInd {
		return maxInd, maxInd
	}
	if p.mat == 0 {
		return int(float32(p.prod()) / (5 + 1/(float32(p.wealth)/100))), 0
	}

	//remainder of production, that must be used to construct both material and industry.
	t := maxInd - p.mat
	return p.mat + int(float32(t)/(5+1/(float32(p.wealth)/100))), p.mat
}

//Growth of a planetary population, through different means.
//Population is less than planet size, population always grows
//by 8 percent per turn, excessive population gets frozen into
//colonists, 8 pops per col, colonists are unfrozen when there
//is free space and are preserved indefinitely on ice.
func (p Planet) Growth() Planet {
	if p.pop == 0 && p.col == 0 {
		return p
	}

	pop := int(1.08 * float32(p.pop))
	if pop > p.size {
		p.col += (pop - p.size) / 8
		p.pop = p.size
		return p
	}
	p.pop = pop

	c := p.col * 8

	d := p.size - p.pop
	if c < d {
		p.col = 0
		p.pop += c
		return p
	}

	p.pop = p.size
	p.col = (c - d) / 8
	return p
}

//Produce desired cargo type and return a planet with new resources
func (p Planet) Produce(w CargoKind) Planet {
	switch w {
	case INDUSTRY:
		tInd, tMat := p.IndProducion()
		p.mat -= tMat
		p.ind += tInd

	case MATERIAL, NONE:
		p.mat += p.wealth * int(p.prod()) / 100

	case COLONISTS:
		panic("Planet can't produce colonists, they are grown naturally. If you are here, things are bad and author should feel bad")
	}
	return p
}
