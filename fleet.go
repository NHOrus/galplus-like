package main

import (
	"math"
	"sort"
)

type GroupID int
type FleetID int

// ShipGroup represent number of ships of certain type in the fleet. Has player-unique ID
type ShipGroup struct {
	Ship
	ID     GroupID
	Amount int
	CType  CargoKind
}

type FleetName string

// Fleet is collection of ships of various sizes, names, technological levels and amounts
type Fleet struct {
	ID       FleetID
	Owner    PlayerID
	Name     FleetName
	Sgs      []ShipGroup
	location Coords
}

// Numbers are total numbers of all ships in the fleet
func (f *Fleet) Numbers() (l int) {
	for _, s := range f.Sgs {
		l += s.Amount
	}
	return l
}

// Trim cuts ship from the fleet whose number dropped down to zero
func (f Fleet) Trim() Fleet {
	ft := Fleet{Owner: f.Owner}
	for i := range f.Sgs {
		if f.Sgs[i].Amount != 0 {
			ft.Sgs = append(ft.Sgs, f.Sgs[i])
		}
	}
	return ft
}

// Sort ship group in fleet to bring same-y ship groups near each other
// May weirldy interprete technological levels
func (f Fleet) Sort() {
	sort.Slice(f.Sgs, func(i, j int) bool {
		return f.Sgs[i].Ship.less(f.Sgs[j].Ship) || f.Sgs[i].CType < f.Sgs[j].CType || f.Sgs[i].Amount < f.Sgs[j].Amount
	})
}

func (f Fleet) equals(g Fleet) bool {
	f.Sort()
	g.Sort()
	if f.Owner != g.Owner {
		return false
	}
	if len(f.Sgs) != len(g.Sgs) {
		return false
	}
	if len(f.Sgs) == 0 {
		return true
	}
	for i := range f.Sgs {
		if !f.Sgs[i].Ship.Equals(g.Sgs[i].Ship) {
			return false
		}
		if f.Sgs[i].Amount != g.Sgs[i].Amount {
			return false
		}
		if f.Sgs[i].CType != g.Sgs[i].CType {
			return false
		}
	}
	return true
}

// Compact returns fleet with same types of ships merged into one group
// TODO: all the bulk inside Cargo needs to be compared somewhere
func (f Fleet) Compact() Fleet {
	f.Sort()
	for i := 0; i < len(f.Sgs)-1; i++ {
		if f.Sgs[i].Ship.Equals(f.Sgs[i+1].Ship) && f.Sgs[i].CType == f.Sgs[i+1].CType {
			f.Sgs[i].Amount += f.Sgs[i+1].Amount
			f.Sgs[i+1].Amount = 0
			if f.Sgs[i].ID > f.Sgs[i+1].ID {
				f.Sgs[i].ID = f.Sgs[i+1].ID
			}
		}
	}
	return f.Trim()
}

// Speed of the fleet is the speed of slowest ship group in the fleet
func (f *Fleet) Speed() Speed {
	s := Speed(math.Inf(1))
	for _, sg := range f.Sgs {
		t := sg.Speed()
		if t == 0 {
			return 0
		}
		if s > t {
			s = t
		}
	}
	return s
}
