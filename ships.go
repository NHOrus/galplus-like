package main

import (
	crand "crypto/rand"
	"encoding/binary"
	"math"
	"math/rand"
)

func init() {
	// Initialize default math/rand source using crypto/rand to provide better
	// security without the performance trade-off.
	buf := make([]byte, 8)
	_, err := crand.Read(buf)
	if err != nil {
		// Failed to read from cryptographic source, fallback to default initial
		// seed (1) by returning early
		return
	}
	seed := binary.BigEndian.Uint64(buf)
	rand.Seed(int64(seed))
}

//Mass of the ship, or ship components
type Mass float64

func (a Mass) equals(b Mass) bool {
	return math.Dim(float64(a), float64(b)) < 0.00001
}

//Speed of the ship, ship group or the fleet, in light years per turn.
type Speed float64

//BaseSpeed of a ship in hyperspace, 20 light years per turn per tech level
const BaseSpeed = 20

//TL contains Technology Levels of the target
type TL struct {
	Weapons float64
	Drive   float64
	Shields float64
	Cargo   float64
}

//Ship is a ship, and there are many of them
type Ship struct {
	TL       TL
	Name     string
	Drive    Mass
	Armament int
	Weapons  Mass
	Shield   Mass
	Cargo    Mass
	hold     Mass
}

//Mass of the ship with empty holds.
func (s *Ship) Mass() Mass {
	t := s.Drive + s.Shield + s.Cargo

	if s.Armament != 0 {
		t += s.Weapons + Mass((s.Armament-1))*s.Weapons/2
	}
	return t
}

//Total mass is ship's effective mass, including cargo in cargo holds
func (s *Ship) Total() (total Mass) {
	s.Mass()
	if s.TL.Cargo != 0 {
		return s.Mass() + s.hold/Mass(s.TL.Cargo)
	}
	return s.Mass()
}

//Surface is ship's surface to be protected by shield, inexplicably increases when there's cargo in holds
func (s *Ship) Surface() float64 {
	return math.Cbrt(float64(s.Total()))
}

func (s *Ship) power() float64 {
	return float64(s.Weapons) * s.TL.Weapons
}

func (s *Ship) defense() float64 {
	//norm is normalization constant to bring ship of Drive: 10, Armaments: 1, Weapons: 10, Shield: 10 and Cargo:0 to defense rating of 10
	const norm = 3.107232505953858866877662427522386362854906829067422001471
	if s.Surface() == 0 {
		return 0
	}
	return float64(s.Shield) * s.TL.Shields * norm / s.Surface()
}

func probability(a, d *Ship) float64 {
	//ln4 is log(4)
	const ln4 = 1.38629436111989061883446424291635313615100026872051050824136001898678724393938943121172665399283737
	if a.Armament == 0 {
		//Whatever power of the guns, if there's zero of them, they are useless
		return 0
	}
	pwr := a.power()
	def := d.defense()
	if pwr >= 4*def {
		return 1
	}
	if def >= 4*pwr {
		return 0
	}
	return (math.Log(pwr/def)*(1/ln4) + 1) / 2
}

//Attack other ship with this one and see if it's destroyed
func (s *Ship) Attack(target *Ship) (destroyed bool) {
	p := probability(s, target)
	roll := rand.Float64()
	return roll < p
}

func (tl TL) less(otl TL) bool {
	if tl.Weapons != otl.Weapons {
		return tl.Weapons < otl.Weapons
	}
	if tl.Drive != otl.Drive {
		return tl.Drive < otl.Drive
	}
	if tl.Shields != otl.Shields {
		return tl.Shields < otl.Shields
	}
	return tl.Cargo < otl.Cargo

}

//Equals is straightforward comparsion of Tech levels
func (tl TL) Equals(otl TL) bool {
	if tl.Weapons != otl.Weapons {
		return false
	}
	if tl.Drive != otl.Drive {
		return false
	}
	if tl.Shields != otl.Shields {
		return false
	}
	return tl.Cargo == otl.Cargo

}

func (s Ship) less(t Ship) bool {
	if s.Name != t.Name {
		return s.Name < t.Name
	}
	if s.power() != t.power() {
		return s.power() < t.power()
	}
	if s.defense() != t.defense() {
		return s.defense() < t.defense()
	}
	if s.Total() != t.Total() {
		return s.Total() < t.Total()
	}
	if s.Weapons != t.Weapons {
		return s.Weapons < t.Weapons
	}
	if s.Drive != t.Drive {
		return s.Drive < t.Drive
	}
	if s.Cargo != t.Cargo {
		return s.Cargo < t.Cargo
	}
	if s.Armament != t.Armament {
		return s.Armament < t.Armament
	}
	return s.TL.less(t.TL)
}

//Equals checks that ships are the same
func (s Ship) Equals(t Ship) bool {
	if s.Name != t.Name {
		return false
	}
	if s.power() != t.power() {
		return false
	}
	if s.defense() != t.defense() {
		return false
	}
	if !s.Total().equals(t.Total()) {
		return false
	}
	if s.Weapons != t.Weapons {
		return false
	}
	if s.Drive != t.Drive {
		return false
	}
	if s.Cargo != t.Cargo {
		return false
	}
	if s.Armament != t.Armament {
		return false
	}
	return s.TL.Equals(t.TL)
}

//Speed is a speed of a ship at current load
//Equals Base speed times Drive level divided by total mass
func (s *Ship) Speed() Speed {
	if s.Drive == 0 {
		return 0
	}
	return Speed(BaseSpeed * s.TL.Drive / float64(s.Total()))
}
