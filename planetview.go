package main

//PlanetView is a view of a planet for player. It is not showing two decimal places at all
type PlanetView struct {
	size   int
	wealth float32
	ID     PlanetID
	Owner  PlayerID
	Name   string
	mat    int
	ind    int
	pop    int
	col    int
}

//View a planet
func (p Planet) View() PlanetView {
	return PlanetView{
		size:   p.size / 100,
		wealth: float32(p.wealth) / 100,
		ID:     p.ID,
		Owner:  p.Owner,
		Name:   p.Name,
		mat:    p.mat / 100,
		ind:    p.ind / 100,
		pop:    p.pop / 100,
		col:    p.col / 100,
	}
}

//ProductView is a view of planetary productivity without decimals
type ProductView float32

func (p Planet) Product() ProductView {
	return ProductView(p.prod()) / 100
}
