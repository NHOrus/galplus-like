package main

import "testing"

func TestPresenceInGalaxy(t *testing.T) {
	distGalaxy := Galaxy{
		Size: Dims{Width: 1000, Height: 1000},
	}
	cases := []struct {
		p   Point
		res bool
	}{
		{Point{0, 0}, true},
		{Point{2000, 0}, false},
		{Point{0, 2000}, false},
		{Point{200, 200}, true},
		{Point{1000, 1000}, false},
		{Point{2000, 2000}, false},
		{Point{999, 999}, true},
	}
	for _, c := range cases {
		if distGalaxy.Size.Contains(c.p) != c.res {
			t.Errorf("Point %+v and dimensions %+v", c.p, distGalaxy.Size)
		}
	}
}

func TestGalaxyDistances(t *testing.T) {
	distGalaxy := Galaxy{
		Size: Dims{Width: 1000, Height: 1000},
	}
	cases := []struct {
		p1, p2 Point
		val    Distance
	}{
		{Point{500, 500}, Point{500, 500}, 0},
		{Point{500, 500}, Point{400, 500}, 100},
		{Point{500, 400}, Point{500, 500}, 100},
		{Point{50, 0}, Point{950, 0}, 100},
		{Point{0, 50}, Point{0, 950}, 100},
		{Point{200, 0}, Point{200, 900}, 100},
		{Point{0, 200}, Point{900, 200}, 100},
		{Point{300, 0}, Point{0, 400}, 500},
		{Point{60, 0}, Point{0, 920}, 100},
		{Point{980, 940}, Point{40, 20}, 100},
	}
	for _, c := range cases {
		if distGalaxy.Size.Distance(c.p1, c.p2) != c.val {
			t.Errorf("Expected distance between points %+v and %+v to be %d, got %d", c.p1, c.p2, c.val, distGalaxy.Size.Distance(c.p1, c.p2))
		}
	}
}
