package main

import (
	"math"
)

//Dims contains dimensions of the galaxy and provides calculations of range modulo dimensions with desired precision
type Dims struct {
	Width  int
	Height int
}

type Distance int

//We cut distances 100 times in visialization. so etha is 0.01 in presentation space - smallest printable decimal distance
const etha = 2

//Point is a point in the galaxy
type Point struct {
	X int
	Y int
}

func (d Dims) Contains(p Point) bool {
	if p.X < 0 || p.X >= d.Width {
		return false
	}
	if p.Y < 0 || p.Y >= d.Height {
		return false
	}
	return true
}

func (d Dims) Distance(a, b Point) Distance {
	dX := a.X - b.X
	if dX < 0 {
		dX = -dX
	}
	dY := a.Y - b.Y
	if dY < 0 {
		dY = -dY
	}

	if dX > d.Height/2 {
		dX = d.Height - dX
	}

	if dY > d.Width/2 {
		dY = d.Width - dY
	}

	return Distance(math.Sqrt((float64)(dX*dX) + (float64)(dY*dY)))
}

func (d Dims) Equals(a, b Point) bool {
	return d.Distance(a, b) < etha
}
