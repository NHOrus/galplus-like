package main

type Coords struct {
	X int64
	Y int64
}

type CoordView struct {
	X float64
	Y float64
}

func (loc Coords) View() CoordView {
	return CoordView{
		X: float64(loc.X) / 100,
		Y: float64(loc.Y) / 100}
}
