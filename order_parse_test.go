package main

import (
	"io/ioutil"
	"testing"
)

const currTurn int = 1

var PlayList = []Player{
	{Name: "Sam", ID: 1},
	{Name: "Bob", ID: 2},
	{Name: "Max", ID: 3},
	{Name: "Green Shadow", ID: 25},
}

var PlanList = []Planet{
	{size: 20000, ID: 1, Owner: 1, Name: "FirstPlanet", wealth: 20, pop: 10000},
	{size: 20000, ID: 2, Owner: 1, wealth: 20, pop: 10000},
	{size: 20000, ID: 3, Owner: 2, Name: "FirstPlanet", wealth: 20, pop: 10000},
}

var TestGalaxy = Galaxy{Name: "test"}

func init() {
	TestGalaxy.Planets = make(map[PlanetID]Planet)
	TestGalaxy.Players = make(map[PlayerID]Player)
	for _, plr := range PlayList {
		TestGalaxy.Players[plr.ID] = plr
	}
	for _, pln := range PlanList {
		TestGalaxy.Planets[pln.ID] = pln
	}
}

func TestParseOrderRenamePlanet(t *testing.T) {
	t.Skip()
	dat, err := ioutil.ReadFile("test/test-Bob-1.ord")
	if err != nil {
		t.Fatal("No test file provided")
	}
	tempGalaxy, nil := TestGalaxy.parseOrderList(dat, 1, 1)
	if err != nil {
		t.Fatal("wot")
	}

	//orders, err = checkOrders(orders)
	newGalaxy := tempGalaxy.applyOrders()

	if newGalaxy.Planets[1].Name != "Home" {
		t.Errorf("Bob was unable to rename his first planet from FirstPlanet to Home")
	}

	if newGalaxy.Planets[2].Name != "Colony" {
		t.Errorf("Bob was unable to name his second planet \"Colony\"")
	}

	if newGalaxy.Planets[3].Name != "FirstPlanet" {
		t.Errorf("Bob was capable of renaming planet of other player")
	}
}
