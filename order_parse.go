package main

import (
	"errors"
	"strconv"
	"strings"
)

const magic string = "#order"

type OrderType int

type item struct {
	kind OrderType
	val  string
}

const (
	ORD_ERROR          OrderType = iota
	ORD_PEACE                    /* A <player name> declare peace with player, if self - peace with everyone */
	ORD_SEPARATE_SHIPS           /* B <group number> [<amount>] pulls amount of ships from group into a new fleet, if none or 0 - whole group is separated */
	ORD_CANCEL                   /* C Cancel all orders for turn before this order */
	ORD_DESIGN                   /* D <ship name> [<Drive> <Armament> <Weapons> <Shields> <Cargo>] Creates new ship type. If no parameters - deletes this ship type if none exist */
	ORD_GIVE                     /* G <ship group number> <player name> [<amount>] gives player ships from a ship group. If amount is zero, whole group is transferred */
	ORD_RESEARCH_PLAN            /* H <research plan name> [<Drive> <Armament> <Weapons> <Shields> <Cargo>] Creates new research plan with tech research priority in proportions. if none, removes research plan */
	ORD_JOIN                     /* J [<group number>|<source fleetname> <target fleetname> [<amount>]] Join same groups. Join (amount of ships from) group to fleet, if possible. Join two ships together, leaving tgt fleet name. */
	ORD_DISMISS                  /* K <group number> [<amount>] Dismiss fleet group or parts of it. Resources are left on planet, except colonists on enemy planet */
	ORD_LOAD                     /* L <group number> <cargo kind> [<amount> [<cargo amount>]] Loads ships with as much cargo as possible, if amount of ships and amount of cargo isn't used, equally spreading cargo over ships */
	ORD_MAP_RESIZE               /* M <X> <Y> <Size> Resize map, centered on position X*Y, square with Size side */
	ORD_RENAME_PLANET            /* N <Planet> <New Name> Renames planet. #PLANET_ID could be used instead of old name */
	ORD_OPTIONS                  /* O GAME OPTIONS NOT IMPLEMENTED*/
	ORD_PRODUCTION               /* P <Planet> <Production> Change production order on the planet.*/
	ORD_QUIT                     /* Q <Galaxy Name> <player name> <password> Set 3-turn quit countdown */
	ORD_ROUTE                    /* R <src planet> <cargo type> [<tgt planet>] Cargo could be MAT, CAP, COL and EMP (empty ships). Automatically moves ships with indicated cargo to tgt planet. Without target planet, route is deleted */
	ORD_MOVE                     /* S <fleet name>|<group number> <tgt planet> [<amount>] Order a move for a fleet, group or part of the group to designated planet */
	ORD_RENAME_SHIP              /* T <ship name> <new name> Rename ship type to join them together later */
	ORD_UNLOAD                   /* U <group number> [<amount> [<cargo amount>]] Unload cargo to a planet. Can't unload colonists to foreign planet */
	ORD_VOTE                     /* V <player name> Give all votes to specified player */
	ORD_WAR                      /* W <player name> declare war to player, if self - war with everyone */
	ORD_UPGDARE_SHIP             /* X <group number> <technology> [<amount> [<max tech level>]] Attempt to upgrade tech level of ships in group to max, limited by number of ships and tech lever restriction */
	ORD_STATUS                   /* Z NOT IMPLEMENTED */

)

func (g Galaxy) parseOrderList(dat []byte, own PlayerID, currTurn int) (Galaxy, error) {
	list := strings.Split(string(dat), "\n")
	err, _, _ := verifyFirstLine(list[0], g.Name)
	if err != nil {
		return g, err
	}
	//	if !g.Players[own].auth(cookie) {
	//		return g, errors.New("Failed to auth user")
	//	}
	return g, nil
}

func verifyFirstLine(line string, gamename string) (e error, cookie string, turn TurnNumber) {
	fields := strings.Fields(line)
	if fields[0] != magic {
		return errors.New("Not an order"), "", 0
	}
	if fields[1] != gamename {
		return errors.New("Wrong galaxy"), "", 0
	}
	cookie = fields[2]
	if strings.EqualFold(fields[3], "turn") && len(fields) >= 5 {
		var t int
		t, e = strconv.Atoi(fields[4])
		turn = TurnNumber(t)
	}
	return
}
