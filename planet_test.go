package main

import (
	"testing"
)

func TestPlanetGrowth(t *testing.T) {
	cases := []struct {
		t string
		s Planet
		e Planet
	}{
		{
			"dead",
			Planet{pop: 0, col: 0},
			Planet{pop: 0, col: 0},
		},
		{
			"colonization",
			Planet{size: 10000, pop: 0, col: 1000},
			Planet{size: 10000, pop: 8000, col: 0},
		},
		{
			"growth",
			Planet{size: 20000, pop: 10000, col: 0},
			Planet{size: 20000, pop: 10800, col: 0},
		},
		{
			"expansion",
			Planet{size: 20000, pop: 10000, col: 1000},
			Planet{size: 20000, pop: 18800, col: 0},
		},
		{
			"overgrowth",
			Planet{size: 20000, pop: 20000, col: 0},
			Planet{size: 20000, pop: 20000, col: 200},
		},
		{
			"overcolonization",
			Planet{size: 20000, pop: 10000, col: 1300},
			Planet{size: 20000, pop: 20000, col: 150},
		},
	}

	for _, c := range cases {
		r := c.s.Growth()
		if r != c.e {
			t.Errorf("In case of %s planetary pops behave unexpectedly: wanted %+v, got %+v", c.t, c.e, r)
		}
	}
}

func TestPlanetProductivity(t *testing.T) {
	cases := []struct {
		pl Planet
		pr Productivity
	}{
		{
			Planet{
				ind: 100000,
				pop: 0,
			}, 0,
		},
		{
			Planet{
				ind: 0,
				pop: 100000,
			}, 25000,
		},
		{
			Planet{
				ind: 100000,
				pop: 50000,
			}, 50000,
		},
		{
			Planet{
				ind: 50000,
				pop: 100000,
			}, 62500,
		},
	}
	for _, c := range cases {
		if c.pl.prod() != c.pr {
			t.Errorf("Planetary productivity is in error, expected %d, got %d on a planet of %+v", c.pr, c.pl.prod(), c.pl)
		}
	}
}

func TestPlanetProductionIndustry(t *testing.T) {
	cases := []struct {
		p Planet
		i int
		m int
	}{
		{
			Planet{
				ind: 0,
				pop: 0,
			}, 0, 0,
		},
		{
			Planet{
				ind: 10000,
				pop: 10000,
				mat: 2000,
			}, 12000, 0,
		},
		{
			Planet{
				ind: 10000,
				pop: 10000,
				mat: 3000,
			}, 12000, 1000,
		},
		{
			Planet{
				ind:    10000,
				pop:    10000,
				mat:    0,
				wealth: 20,
			}, 11000, 0,
		},
		{
			Planet{
				ind:    10000,
				pop:    10000,
				mat:    1000,
				wealth: 20,
			}, 11100, 0,
		},
	}

	for _, c := range cases {
		rp := c.p.Produce(INDUSTRY)
		if rp.ind != c.i {
			t.Errorf("Planet doesn't procude right amount of industry, expected %d, got %d from a planet of %+v", c.i, rp.ind, c.p)
		}
		if rp.mat != c.m {
			t.Errorf("Planet doesn't consume right amount of material, expected %d, got %d from a planet of %+v", c.m, rp.mat, c.p)
		}
	}
}

func TestPlanetProductionMaterial(t *testing.T) {
	cases := []struct {
		p Planet
		m int
	}{
		{
			Planet{
				ind: 0,
				pop: 0,
			}, 0,
		},
		{
			Planet{
				ind:    0,
				pop:    0,
				wealth: 10,
			}, 0,
		},
		{
			Planet{
				ind:    10000,
				pop:    10000,
				wealth: 10,
			}, 1000,
		},
		{
			Planet{
				ind:    10000,
				pop:    10000,
				wealth: 150,
			}, 15000,
		},
		{
			Planet{
				ind:    10000,
				pop:    10000,
				wealth: 2000,
			}, 200000,
		},
	}

	for _, c := range cases {
		rp := c.p.Produce(MATERIAL)
		if rp.mat != c.m {
			t.Errorf("Planet doesn't produce right amount of material, expected %d, got %d from a planet of %+v", c.m, rp.mat, c.p)
		}
	}
}
