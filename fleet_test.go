package main

import (
	"testing"
)

var Alice = Player{
	ID:   1,
	Name: "Alice",
}

var Bob = Player{
	Name: "Bob",
	ID:   2,
}
var tech = TL{
	Weapons: 1,
	Shields: 1,
	Drive:   1,
}

var tech2 = TL{
	Weapons: 2,
	Shields: 2,
	Drive:   2,
}

var attacker = Ship{
	TL:       tech,
	Name:     "Attacker",
	Armament: 1,
	Weapons:  10}

var cover = Ship{
	TL:    tech,
	Name:  "Popcorn",
	Drive: 1,
}
var cover2 = Ship{
	TL:    tech2,
	Name:  "Popcorn",
	Drive: 1,
}

var armoredcover = Ship{
	TL:     tech,
	Name:   "Popcorn",
	Drive:  1,
	Shield: 1}

var heavycover = Ship{
	TL:     tech,
	Name:   "Popcorn",
	Shield: 10}

var cleaner = Ship{
	TL:       tech,
	Name:     "Multigun",
	Armament: 10,
	Weapons:  1,
}

var cargo = Ship{
	TL:     tech,
	Name:   "Hauler",
	Drive:  10,
	Shield: 1,
	Cargo:  10,
}

func TestFleetNumbers(t *testing.T) {
	tmp := Fleet{Sgs: []ShipGroup{{Amount: 0}, {Amount: 0}, {Amount: 0}}}
	l := tmp.Numbers()
	if l != 0 {
		t.Errorf("Empty fleet should have 0 ships, but has %d", l)
	}
	tmp = Fleet{Sgs: []ShipGroup{{Amount: 1}, {Amount: 100}, {Amount: 24}}}

	l = tmp.Numbers()
	if l != 125 {
		t.Errorf("Fleet should have 125 ships, but has %d", l)
	}
}

func TestFleetTrim(t *testing.T) {
	s := Fleet{Sgs: make([]ShipGroup, 10)}
	for i := range s.Sgs {
		s.Sgs[i] = ShipGroup{Amount: 0}
	}
	s = s.Trim()
	if len(s.Sgs) != 0 {
		t.Errorf("Fleet must be trimmed to empty, not to %+v", s)
	}
	s = Fleet{Sgs: []ShipGroup{{Amount: 2}, {Amount: 0}, {Amount: 4}}}
	s = s.Trim()
	if len(s.Sgs) != 2 && s.Numbers() != 6 {
		t.Errorf("Fleet trimmed incorrectly, must be 8 ships of 2 classes, got %+v", s)
	}

	s = Fleet{Sgs: []ShipGroup{{Amount: 0}, {Amount: 2}, {Amount: 0}, {Amount: 4}, {Amount: 0}, {Amount: 4}, {Amount: 0}, {Amount: 0}, {Amount: 0}, {Amount: 4}, {Amount: 0}, {Amount: 0}}}
	s = s.Trim()
	if len(s.Sgs) != 4 && s.Numbers() != 14 {
		t.Errorf("Fleet trimmed incorrectly, must be 14 ships of 4 classes, got %+v", s)
	}
}

func TestFleetSort(t *testing.T) {
	cases := []struct {
		input, output Fleet
	}{
		{Fleet{}, Fleet{}},
		{
			Fleet{Sgs: []ShipGroup{
				{attacker, 1, 10, NONE},
			}},
			Fleet{Sgs: []ShipGroup{
				{attacker, 2, 10, NONE},
			}},
		},
		{
			Fleet{Sgs: []ShipGroup{
				{attacker, 3, 10, NONE}, {cover, 4, 1, NONE},
			}},
			Fleet{Sgs: []ShipGroup{
				{cover, 5, 1, NONE}, {attacker, 6, 10, NONE},
			}},
		},
	}
	for _, c := range cases {
		c.input.Sort()
		if !c.input.equals(c.output) {
			t.Errorf("Sorting is broken, want %+v, got %+v", c.input, c.output)
		}
	}
}

func TestFleetEquals(t *testing.T) {
	cases := []struct {
		a, b   Fleet
		result bool
	}{
		{
			Fleet{},
			Fleet{Sgs: []ShipGroup{{}, {}}},
			false,
		},
		{
			Fleet{Sgs: []ShipGroup{{}}},
			Fleet{Sgs: []ShipGroup{{}}},
			true,
		},
		{
			Fleet{Owner: Alice.ID},
			Fleet{Owner: Bob.ID},
			false,
		},
		{
			Fleet{Sgs: []ShipGroup{{cover, 1, 20, NONE}}},
			Fleet{Sgs: []ShipGroup{{cover2, 2, 20, NONE}}},
			false,
		},
		{
			Fleet{Sgs: []ShipGroup{{cover, 1, 20, NONE}}},
			Fleet{Sgs: []ShipGroup{{cover, 2, 10, NONE}}},
			false,
		},
		{
			Fleet{Sgs: []ShipGroup{{cargo, 1, 20, NONE}}},
			Fleet{Sgs: []ShipGroup{{cargo, 2, 20, COLONISTS}}},
			false,
		},
		{
			Fleet{Sgs: []ShipGroup{{cover, 1, 20, NONE}}},
			Fleet{Sgs: []ShipGroup{{cover2, 2, 20, NONE}}},
			false,
		},
		{
			Fleet{},
			Fleet{},
			true,
		},
		{
			Fleet{Owner: Alice.ID},
			Fleet{Owner: Alice.ID},
			true,
		},
	}

	for _, c := range cases {
		cmp := c.a.equals(c.b)
		if cmp != c.result {
			t.Errorf("Comparison fails between %+v, %+v, because their equality is %t not %t", c.a, c.b, cmp, c.result)
		}
	}
}
func TestFleetCompact(t *testing.T) {
	cases := []struct {
		sample Fleet
		result Fleet
	}{
		{Fleet{}, Fleet{}},
		{
			Fleet{Sgs: []ShipGroup{{cover, 1, 10, NONE}, {cover, 2, 10, NONE}}},
			Fleet{Sgs: []ShipGroup{{cover, 1, 20, NONE}}},
		},
		{
			Fleet{Sgs: []ShipGroup{{cover, 1, 10, NONE}, {cleaner, 2, 1, NONE}, {cover, 3, 10, NONE}}},
			Fleet{Sgs: []ShipGroup{{cover, 1, 20, NONE}, {cleaner, 2, 1, NONE}}},
		},
		{
			Fleet{Sgs: []ShipGroup{{cargo, 3, 10, NONE}, {attacker, 2, 1, NONE}, {cargo, 1, 10, INDUSTRY}, {cargo, 4, 1, NONE}}},
			Fleet{Sgs: []ShipGroup{{attacker, 2, 1, NONE}, {cargo, 1, 11, NONE}, {cargo, 3, 10, INDUSTRY}}},
		},
	}

	for _, c := range cases {
		r := c.sample.Compact()
		if !r.equals(c.result) {
			t.Errorf("Compaction went wrong, with result %+v instead of %+v", r, c.result)
		}
	}
}

func TestFleetSpeed(t *testing.T) {
	cases := []struct {
		sample Fleet
		result Speed
	}{
		{Fleet{Sgs: []ShipGroup{{}}}, 0},
		{Fleet{Sgs: []ShipGroup{{cover, 1, 1, NONE}}}, 20},
		{Fleet{Sgs: []ShipGroup{{cover2, 2, 1, NONE}}}, 40},
		{Fleet{Sgs: []ShipGroup{{cover2, 3, 1, NONE}, {cover, 4, 1, NONE}}}, 20},
		{Fleet{Sgs: []ShipGroup{{armoredcover, 5, 1, NONE}, {cover, 6, 1, NONE}}}, 10},
	}

	for _, c := range cases {
		r := c.sample.Speed()
		if r != c.result {
			t.Errorf("Expected speed of %f, got %f for fleet %+v", c.result, r, c.sample)
		}
	}
}
