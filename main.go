package main

import (
	"fmt"
	"log"
	"text/tabwriter"

	"github.com/jroimartin/gocui"
)

var p = Planet{
	Name:   "Test",
	size:   1000,
	wealth: 10,
}

func main() {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.Cursor = true

	g.SetManagerFunc(layout)

	if err := keybindings(g); err != nil {
		log.Panicln(err)
	}

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()
	if v, err := g.SetView("planets", -1, -1, 30, maxY-2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Highlight = true
		v.SelBgColor = gocui.ColorGreen
		v.SelFgColor = gocui.ColorBlack
		if _, err := g.SetCurrentView("planets"); err != nil {
			return err
		}
	}
	if v, err := g.SetView("planet", 30, -1, maxX, maxY-2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		if p.View().display(v) != nil {
			return err
		}

	}

	return nil
}

func (pv PlanetView) display(v *gocui.View) error {
	w := tabwriter.NewWriter(v, 0, 0, 1, ' ', 0)
	_, _ = fmt.Fprintf(w, "%s:\t\t#%d\t\n", pv.Name, pv.ID)
	_, _ = fmt.Fprintf(w, "\tsize:\t%d\t\n", pv.size)
	_, _ = fmt.Fprintf(w, "\twealth:\t%.1f\t\n", pv.wealth)
	return w.Flush()
}

func keybindings(g *gocui.Gui) error {
	return g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit)
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}
